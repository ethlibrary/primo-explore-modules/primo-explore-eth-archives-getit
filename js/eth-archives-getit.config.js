export const ethArchivesGetitConfig = function(){
    return {
        hsa: {
            label: {
                linktext1: {
                    de: 'Bestellen über Hochschularchiv Online',
                    en: 'Request via Hochschularchiv Online'
                },
                text1:{
                    de: 'Informationen zu Bestellung und Benutzung siehe',
                    en: 'See'
                },
                linktext2: {
                    de: 'Bedienungshinweise',
                    en: 'manual (in German)'
                },
                text2:{
                    de: '.',
                    en: 'for more information about requests.'

                }
            },
            url:{
                content: {
                    de: 'http://archivdatenbank-online.ethz.ch/hsa/#/content/',
                    en: 'http://archivdatenbank-online.ethz.ch/hsa/#/content/'
                },
                manual: {
                    de: 'http://archivdatenbank-online.ethz.ch/hsa/#/manual',
                    en: 'http://archivdatenbank-online.ethz.ch/hsa/#/manual'
                }
            }
        },
        mfa:{
            label: {
                linktext1: {
                    de: 'Bestellen über Max Frisch-Archiv Online',
                    en: 'Request via Max Frisch-Archiv Online'
                },
                text1:{
                    de: 'Informationen zu Bestellung siehe',
                    en: 'See'
                },
                linktext2: {
                    de: 'Bedienungshinweise',
                    en: 'manual (in German)'
                },
                text2:{
                    de: '.',
                    en: 'for more information about requests.'
                }
            },
            url:{
                content: {
                    de: 'http://maxfrischarchiv-online.ethz.ch/home/#/content/',
                    en: 'http://maxfrischarchiv-online.ethz.ch/home/#/content/'
                },
                manual: {
                    de: 'http://maxfrischarchiv-online.ethz.ch/home/#/manual',
                    en: 'http://maxfrischarchiv-online.ethz.ch/home/#/manual'
                }
            }
        },
        tma:{
            label: {
                linktext1: {
                    de: 'Detailinformationen in Thomas Mann-Archiv Online (nur Metadaten)',
                    en: 'Detailed information in Thomas Mann-Archiv Online (metadata only)'
                },
                text1:{
                    de: 'Keine Bestellung möglich. Das Digitalisat kann im Lesesaal des Thomas-Mann-Archivs auf Voranmeldung eingesehen werden (',
                    en: 'No request possible. The digitized item can be consulted in the Thomas Mann Archives reading room upon advance registration ('
                },
                linktext2: {
                    de: 'Kontakt',
                    en: 'Contact'
                },
                text2:{
                    de: ').',
                    en: ').'
                }
            },
            url:{
                content: {
                    de: 'http://www.online.tma.ethz.ch/home/#/content/',
                    en: 'http://www.online.tma.ethz.ch/home/#/content/'
                },
                contact: {
                    de: 'https://tma.ethz.ch/utils/kontakt.html',
                    en: 'https://tma.ethz.ch/en/utils/contact.html'
                }
            }
        }

    }
}
